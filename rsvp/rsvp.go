package rsvp

type Rsvp struct {
	Name, Email, Phone string
	WillAttend         bool
}

var Responses = make([]*Rsvp, 0, 10)

type FormData struct {
	*Rsvp
	Errors []string
}
