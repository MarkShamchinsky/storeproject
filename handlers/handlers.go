package handlers

import (
	"net/http"
	"storeproject/rsvp"
	"storeproject/templates"
)

func WelcomeHandler(writer http.ResponseWriter, request *http.Request) {
	templates.Templates["welcome"].Execute(writer, nil)
}
func ListHandler(writer http.ResponseWriter, request *http.Request) {
	templates.Templates["list"].Execute(writer, rsvp.Responses)
}

func FormHandler(writer http.ResponseWriter, request *http.Request) {
	if request.Method == http.MethodGet {
		templates.Templates["form"].Execute(writer, rsvp.FormData{
			Rsvp: &rsvp.Rsvp{}, Errors: []string{},
		})
	}
}
