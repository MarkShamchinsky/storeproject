package main

import (
	"fmt"
	"net/http"
	"storeproject/handlers"
	"storeproject/templates"
)

func main() {
	templates.LoadTemplates()
	http.HandleFunc("/", handlers.WelcomeHandler)
	http.HandleFunc("/list", handlers.ListHandler)
	http.HandleFunc("/form", handlers.FormHandler)
	err := http.ListenAndServe(":8081", nil)

	if err != nil {

		fmt.Println(err)

	}
}
